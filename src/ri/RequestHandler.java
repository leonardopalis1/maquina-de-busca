package ri;
import java.io.IOException;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
 
public class RequestHandler {
	static RequestHandler handler;

	public Document getDataUrl(String url) throws Exception{
            try{
                Connection conn = Jsoup.connect(url);
		Document doc =  conn.get();
                Response response = conn.execute();
                
                if ( response.hasHeader("location") ){
                    Fetcher.isRedirect = true;
                }else{
                    Fetcher.isRedirect = false;
                    Execution.volumeCounter += response.bodyAsBytes().length;
                }
                
                return doc;       
            }catch (Exception e){
                return null;
            }
	}
        
        public static RequestHandler getInstance(){
		
            if(handler == null){ 
                handler = new RequestHandler(); 
            }
		return handler;
	}        
        
        public boolean isRedirect(String url) {
            boolean resp = true;
            try{
            Response response = Jsoup.connect(url).execute();

            if(response.hasHeader("location")){
                resp = true;
            }
            resp = false;
            }catch ( Exception e ){
                
            }finally{
                return resp;
            }
        }
	
	
	
}