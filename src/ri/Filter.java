package ri;

import java.util.HashMap;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Leonardo, Felipe
 */
public class Filter {
    private static HashMap<String,Document> localCorpus;
    private static  Schedule instanceSchedule;
    private static Parser instanceParser;
    
    public Filter(Schedule instanceSchedule, Parser instanceParser){
        this.instanceSchedule = instanceSchedule;
        this.instanceParser = instanceParser;
        localCorpus = new HashMap<>( );
    }
    
    public static void filter ( String url, Document webPage ){
            if ( webPage != null ){
                if ( (localCorpus.containsKey(url) == false 
                || localCorpus.get(url).equals(webPage) == false)
                && (localCorpus.size() < 10)   ){
                    localCorpus.put(url, webPage);  
                }
                
                for (Element link : instanceParser.getParsedLinks()) {
                    if ( localCorpus.containsKey(link.attr("abs:href")) == false 
                            && instanceSchedule.getScheduleLinks().contains(link) == false ){
                        instanceSchedule.getScheduleLinks().add(link);
                    }
                }
                instanceParser.getParsedLinks().removeAll(instanceParser.getParsedLinks());
            }
        }
    
    
}
