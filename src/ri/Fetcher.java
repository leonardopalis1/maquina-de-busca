/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ri;

import java.rmi.UnknownHostException;
import org.jsoup.nodes.Document;
/**
 *
 * @author Leonardo, Felipe
 */
public class Fetcher {
    private static Output output;
    private static int contador;
    public static boolean isRedirect;
    public Fetcher(){
        this.contador = 0;
        output = new Output();
    }
    public static Document fetch ( String url )throws Exception {
            Document webPage = null;
            isRedirect = false;
            
            try{
                webPage = RequestHandler.getInstance().getDataUrl(url);
                
                if ( isRedirect == false ) {
                     Execution.fetchCounter++;
                }
                output.message("Paginas Coletadas:", "" + contador);
            }catch(UnknownHostException  err){
                output.error("ERROR", "DNS record not found");
            }
            
            return webPage;
        }
        
    
}
