/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ri;

import org.jsoup.select.Elements;

/**
 *
 * @author Leonardo, Felipe
 */
public class Schedule {
     public static String url;
     private static Elements scheduledLinks;
     
    public Schedule(String url){
        this.url = url;
        scheduledLinks = new Elements();
    }
    
    public Elements getScheduleLinks(){
        return scheduledLinks;
    }
    
    public static String start ( ) throws Exception {
        if (scheduledLinks.isEmpty() == false){
            return url = scheduledLinks.remove(0).attr("abs:href");
        }else{
            return "";
        }
       
    }
}
