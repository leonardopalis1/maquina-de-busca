/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ri;
import ri.Output;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/**
 *
 * @author Leonardo, Felipe
 */
public class Parser {
	private static Output output;
        private static Elements parsedLinks;
        public Parser(){
            output = new Output();
            parsedLinks = new Elements();
        }
        
        public Elements getParsedLinks(){
            return parsedLinks;
        }
	public static Elements parse( Document webPage )  {
                Elements links = null;
                
                try{
                   if ( webPage != null ){ 
                    links = webPage.select("a[href]");
                   }
                    
                }catch( Exception e ){ e.printStackTrace(); 
                }finally{
                   if ( links == null ) {
                       output.message("Links:", "0");
                   }else{
                       output.message("Links:", Integer.toString(links.size()));
                       for (Element link : links) {
                           if ( link != null){
                               parsedLinks.add(link);
                               //output.message("Find: ", link.attr("abs:href"));
                           }
                           
                       }
                   }
                }
                return links;
        }
}