/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ri;
import ri.Output;
import java.net.InetAddress;
import java.net.URI;
import java.rmi.UnknownHostException;
import java.util.HashMap;
import sun.net.spi.nameservice.dns.DNSNameService;

/**
 *
 * @author Leonardo, Felipe
 */
public class DNS {
    private static Output output;
    private static HashMap<String, InetAddress> addr;
    public DNS(){
        output = new Output();
        addr = new HashMap<>();
        
    }
    public static String resolveAddr(URI hostName) {
        try{
            String normalizedURL = resolve(hostName);
            return normalizeURL(normalizedURL);
        }catch ( Exception e ){
            return "";
        }
        
    }
    
    private static void indexAddr(InetAddress address, URI hostName){
        addr.put(hostName.toString(), address);
        logInfo(address);
    }
    
    private static void logInfo(InetAddress address) {
        output.message("DNS", "Host IP Address: " + address.getHostAddress());
        output.message("DNS", "Host IP Address: " + address.getHostName());
    }
    
    private static String resolve(URI hostName) throws Exception {
        try{

            DNSNameService service = new DNSNameService();
            InetAddress ipAddress = InetAddress.getByName( hostName.toString().replaceFirst("http://","")  ); 
            indexAddr(ipAddress, hostName);
            return ipAddress.getHostAddress();
        }catch (UnknownHostException e){
                output.message("DNS:","Invalid HostName");
                return "";
        }catch(Exception e){
                return "";
        }

    }
    
     public static String normalizeURL ( String url ){
            
            url = url.replace("https", "http");
            if (url.contains("http://") == false) {
                url = "http://"+url;
            }

            return url;
        }

}
